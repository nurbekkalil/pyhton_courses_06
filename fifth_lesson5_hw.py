# Напишите пример функции возвращающей значения с помощью return


my_friends = [
    {
        'name': 'Oskar',
        'age': 18,
        'gender': 'male',

    },

    {
        'name': 'Mer',
        'age': 22,
        'gender': 'female',

    },

    {
        'name': 'Zaka',
        'age': 21,
        'gender': 'male',

    },

    {
        'name': 'Aizada',
        'age': 23,
        'gender': 'female',
    }
]

def ohrannik(clients):
    approved_people = []

    for client in clients:
        if client['age'] >= 21:
            approved_people.append(client)

    return approved_people

result = ohrannik(my_friends)

print(result)






# Напишите пример клуба с двумя функциями (функция охранник, функция администратор), функция охранника должна принимать в себя любое количество массивов с людьми
my_friends = [
    {
        'name': 'Oskar',
        'age': 18,
        'gender': 'male',

    },

    {
        'name': 'Mer',
        'age': 22,
        'gender': 'female',

    },

    {
        'name': 'Zaka',
        'age': 21,
        'gender': 'male',

    },

    {
        'name': 'Aizada',
        'age': 23,
        'gender': 'female',
    }
]

def ohrannik(clients):
    approved_people = []

    for client in clients:
        if client['age'] >= 21:
            approved_people.append(client)

    return approved_people

def administrator(clients):
    for client in clients:
        client['status'] = True



proshli_v_klub = ohrannik(my_friends)
administrator(proshli_v_klub)

for friend in my_friends:
    print(friend)




# Напишите пример цикла в цикле


my_friends = [
    {
        'name': 'Oskar',
        'age': 18,
        'gender': 'male',

    },

    {
        'name': 'Mer',
        'age': 22,
        'gender': 'female',

    },

    {
        'name': 'Zaka',
        'age': 21,
        'gender': 'male',

    },

    {
        'name': 'Aizada',
        'age': 22,
        'gender': 'female',
    }
]

my_friends_2 = [

    {
        'name': 'Oskar2',
        'age': 18,
        'gender': 'male',

    },

    {
        'name': 'Mer2',
        'age': 22,
        'gender': 'female',

    },

    {
        'name': 'Zaka2',
        'age': 21,
        'gender': 'male',

    },

    {
        'name': 'Aizada2',
        'age': 22,
        'gender': 'female',
    }
]


def ohrannik(*args):

    approved_people = []

    for druzya in args:
        for drug in druzya:
            if drug['age'] >= 21:
                approved_people.append(drug)

    return approved_people

result_ohrannika = ohrannik(my_friends, my_friends_2)

print(result_ohrannika)
