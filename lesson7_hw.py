#frozenset
my_set = {1, 2, 3, 4, 5}
my_frozenset = frozenset(my_set)

print(my_set)

#add
my_set = {1, 2, 3, 4, 5}

my_set.add(6)
print(my_set)

#clear
my_set = {1, 2, 3, 4, 5}

my_set.clear()
print(my_set)

#copy
my_set = {1, 2, 3, 4, 5}

my_set_1 = my_set.copy()

print(my_set_1)

#difference
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set_1.difference(my_set))
print(my_set.difference(my_set_1))

# Discard
my_set = {1, 2, 3, 4, 5}

my_set.discard(3)
print(my_set)

#intersection
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.intersection(my_set_1))

#pop
my_set = {1, 2, 3, 4, 5}
my_set.pop()

print(my_set)

#symmetric_difference()
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.symmetric_difference(my_set_1))

#isdisjoint
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.isdisjoint(my_set_1))

my_set = {1, 2, 3, 4, 5}
my_set_1 = {6, 7, 8, 9, 10}

print(my_set.isdisjoint(my_set_1))

#issubset
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.issubset(my_set_1))

my_set = {2, 5, 6}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.issubset(my_set_1))

#issuperset
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.issubset(my_set_1))

my_set = {2, 5, 6, 9, 3}
my_set_1 = {1, 2, 3, 4, 5, 6, 9}

print(my_set.issubset(my_set_1))

#union
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.union(my_set_1))

#set update
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}

print(my_set.update(my_set_1))
print(my_set)

#intersectionupdate
my_set = {1, 2, 3, 4, 5}
my_set_1 = {2, 5, 6, 9, 3}
print(my_set.intersection_update(my_set_1))
print(my_set)

#continue

my_list = list(range(0, 150))
for i in my_list:
    if i > 70:
        continue
    print(i)


#break

my_list = list(range(0, 150))
for i in my_list:
    print('--- number ---', i)
    break
