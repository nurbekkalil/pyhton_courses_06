# Напишите пример содержащий функцию принимающую *args

def argument(*args):
    result = 0

    for argument in args:
        result += argument

    return result

result = argument(11, 23, 33, 41,)

print(result)

# Напишите пример содержащий функцию принимающую *kwargs

def imen_argument(**kwags):
    print('--- argumet ---', kwags)

imen_argument(Oskar=27, Zaka=25, Mer=23)


# Напишите пример содержащий функцию принимающую *args и *kwargs

def argumenty(*args, **kwargs):
    print('-- argument --', args)
    print('-- imen argument --', kwargs)

argumenty('Aizada', 'Oskar', 1, 2)
argumenty(Meka=22, Zaka=26)

# Напишите пример содержащий функцию принимающую *args и *kwargs а также простые агрументы

def argumenty(*args, **kwargs):
    print('-- argument --', args)
    print('-- imen argument --', kwargs)

argumenty(1, 2, 3, 4)
argumenty(Meka=22, Zaka=26)
argumenty()
argumenty(7, 8, 9, Oskar=25, Mer=22)


# Напишите пример lambda функции которая возвращает результат умножения числа пришедшего в аргументы на 5

lamba_function_joke = lambda number: number * 103

result = lamba_function_joke(5)
print(result)

# Напишите пример lambda функции которая возвращает результат деления числа пришедшего в аргументы на 5

lamba_function_joke = lambda number: number / 2

result = lamba_function_joke(5)
print(result)

# Напишите пример lambda функции которая возвращает значение 'age' из пришедшего в аргументы словаря

my_list = [
    {
        'name': 'Oskar',
        'age': 25

    },

    {
        'name': 'Zaka',
        'age': 24

    },
    {
        'name': 'Mer',
        'age': 22

    }

]


my_list.sort(key=lambda chelovek: chelovek['age'])

print(my_list)



# list

my_list = ['Nurbek', 'Oskar', 'Zaka', 'Mer', 'Meka']

print(my_list)


#
my_list = ['Nurbek', 'Oskar', 'Zaka', 'Mer']

print(my_list[2])

#
my_list = ['Nurbek', 'Oskar', 'Zaka', 'Mer']

my_list.append('Meka')

print(my_list)

#

my_list = ['Nurbek', 'Oskar', 'Zaka', 'Mer']

my_list.pop(1)
print(my_list)

# dict

dict = {
    'Nurbek': 26,
    'Zaka': 23,
    'Oskar': 24
}
print(dict)

#

dict = {
    'Nurbek': 26,
    'Zaka': 23,
    'Oskar': 24
}

dict['Nurbek'] = 22
dict['Meka'] = 28

print(dict)

# tuple


tuple = ('Nurbek', 'Oskar', 'Zaka')

print(tuple)

#

my_tuple = ('Nurbek', 'Oskar', 'Zaka', 'Mer')

my_tuple = list(my_tuple)

my_tuple.pop(0)
print(my_tuple)


# Напишите пример примеия lambda функции в методе list (sort)

my_list = [1, 3, 5, 2, 4]

my_list.sort(key=lambda number: number)

print(my_list)


#set


my_set = {'Nurbek', 'Oskar', 'Zaka', 'Mery'}

print(my_set)

